<?php
declare(strict_types=1);

// path to where svg folder from twemoji repository lies
// https://github.com/twitter/twemoji/tree/master/assets/svg
$input = __DIR__ . DIRECTORY_SEPARATOR . 'svg' . DIRECTORY_SEPARATOR;
$output = __DIR__ . DIRECTORY_SEPARATOR . 'flags' . DIRECTORY_SEPARATOR;

if( !file_exists( $input ) )
{
	echo 'Input folder does not exist: ' . $input . PHP_EOL;
	exit( 1 );
}

if( !file_exists( $output ) )
{
	echo 'Creating output folder: ' . $output . PHP_EOL;
	mkdir( $output );
}

for( $a = 65; $a <= 90; $a++ )
{
	$unicodeA = dechex( 127397 + $a );

	for( $b = 65; $b <= 90; $b++ )
	{
		$unicodeB = dechex( 127397 + $b );
		$path = $input . $unicodeA . '-' . $unicodeB . '.svg';

		if( !file_exists( $path ) )
		{
			continue;
		}

		$name = chr( $a ) . chr( $b );

		echo 'Found ' . $name . PHP_EOL;

		copy( $path, $output . $name . '.svg' );
	}
}
